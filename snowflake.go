package snowflake

import (
	"errors"
	"strconv"
	"sync/atomic"
	"time"
)

var (
	// Epoch is set to the twitter snowflake epoch of Nov 04 2010 01:42:54 UTC in milliseconds
	// You may customize this to set a different epoch for your application.
	Epoch int64 = time.Date(2020, 1, 0, 0, 0, 0, 0, time.UTC).UnixNano() / int64(time.Millisecond)

	TimeBits uint8 = 41

	// NodeBits holds the number of bits to use for Node
	// Remember, you have a total 22 bits to share between Node/Step
	NodeBits uint8 = 10

	// StepBits holds the number of bits to use for Step
	// Remember, you have a total 22 bits to share between Node/Step
	StepBits uint8 = 12

	NodeMax int64 = -1 ^ (-1 << NodeBits)
	TimeShift uint8 = NodeBits + StepBits
	NodeShift uint8 = StepBits

	TimeMask int64 = -1 ^ (-1 << TimeBits)
	NodeMask int64 = NodeMax << StepBits
	StepMask int64 = -1 ^ (-1 << StepBits)
)


// A Node struct holds the basic information needed for a snowflake generator
// node
type Node struct {
	//epoch time.Time
	epoch int64
	time  int64
	node  int64
	step  int64
}

// NewNode returns a new snowflake node that can be used to generate snowflake
// IDs
func NewNode(node int64) (*Node, error) {

	if node < 0 || node > NodeMax {
		return nil, errors.New("Node number must be between 0 and " + strconv.FormatInt(NodeMax, 10))
	}

	return &Node{
		epoch: Epoch,
		node: node,
		time: 0,
		step: -1,
	}, nil
}

// Generate creates and returns a unique snowflake ID
// To help guarantee uniqueness
// - Make sure your system is keeping accurate system time
// - Make sure you never have multiple nodes running with the same node ID
func (n *Node) Generate() SnowFlake {

	var now int64
	var step int64 = 0
	for {
		tmp := atomic.LoadInt64(&n.time)
		now = int64(time.Now().UnixNano()) / 1000000
		if atomic.CompareAndSwapInt64(&n.time, tmp, now) {
			if tmp == now {
				step = atomic.AddInt64(&n.step, 1)
			}else{
				atomic.SwapInt64(&n.step, 0)
			}
			break
		}
	}

	return SnowFlake((now - n.epoch) << TimeShift | (n.node << NodeShift) | (step))
}
