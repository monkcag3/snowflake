package snowflake

import (
	"encoding/base64"
	"encoding/binary"
	"strconv"
)

type SnowFlake int64


// returns an int64 of the snowflake ID
func (f SnowFlake) Int64() int64 {
	return int64(f)
}

// converts an int64 into a snowflake ID
func FromInt64(id int64) SnowFlake {
	return SnowFlake(id)
}

// String returns a string of the snowflake ID
func (f SnowFlake) String() string {
	return strconv.FormatInt(int64(f), 10)
}

// ParseString converts a string into a snowflake ID
func FromString(id string) (SnowFlake, error) {
	i, err := strconv.ParseInt(id, 10, 64)
	return SnowFlake(i), err
}

// Base2 returns a string base2 of the snowflake ID
func (f SnowFlake) Base2() string {
	return strconv.FormatInt(int64(f), 2)
}

// FromBase2 converts a Base2 string into a snowflake ID
func FromBase2(id string) (SnowFlake, error) {
	i, err := strconv.ParseInt(id, 2, 64)
	return SnowFlake(i), err
}


// Base36 returns a base36 string of the snowflake ID
func (f SnowFlake) Base36() string {
	return strconv.FormatInt(int64(f), 36)
}

// FromBase36 converts a Base36 string into a snowflake ID
func FromBase36(id string) (SnowFlake, error) {
	i, err := strconv.ParseInt(id, 36, 64)
	return SnowFlake(i), err
}


// Base64 returns a base64 string of the snowflake ID
func (f SnowFlake) Base64() string {
	return base64.StdEncoding.EncodeToString(f.Bytes())
}

// FromBase64 converts a base64 string into a snowflake ID
func FromBase64(id string) (SnowFlake, error) {
	b, err := base64.StdEncoding.DecodeString(id)
	if err != nil {
		return -1, err
	}
	return FromBytes(b)

}

// Bytes returns a byte slice of the snowflake ID
func (f SnowFlake) Bytes() []byte {
	return []byte(f.String())
}

// FromBytes converts a byte slice into a snowflake ID
func FromBytes(id []byte) (SnowFlake, error) {
	i, err := strconv.ParseInt(string(id), 10, 64)
	return SnowFlake(i), err
}

// IntBytes returns an array of bytes of the snowflake ID, encoded as a
// big endian integer.
func (f SnowFlake) IntBytes() [8]byte {
	var b [8]byte
	binary.BigEndian.PutUint64(b[:], uint64(f))
	return b
}

// ParseIntBytes converts an array of bytes encoded as big endian integer as
// a snowflake ID
func ParseIntBytes(id [8]byte) SnowFlake {
	return SnowFlake(int64(binary.BigEndian.Uint64(id[:])))
}

// Time returns an int64 unix timestamp in milliseconds of the snowflake ID time
func (f SnowFlake) Time() int64 {
	return (int64(f) >> TimeShift) + Epoch
}

// Node returns an int64 of the snowflake ID node number
func (f SnowFlake) Node() int64 {
	return int64(f) & NodeMask >> NodeShift
}

// Step returns an int64 of the snowflake step (or sequence) number
func (f SnowFlake) Sequence() int64 {
	return int64(f) & StepMask
}
