# snowflake

#### 介绍
derive from https://github.com/bwmarrin/snowflake

#### 软件架构
使用原子操作代替原有的锁

#### 测试
 go test -bench="."

![输入图片说明](https://images.gitee.com/uploads/images/2020/0629/130430_a2c4fcb3_1171339.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0629/130300_f423b1ce_1171339.png "屏幕截图.png")