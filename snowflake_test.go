package snowflake

import (
	"testing"
)

func BenchmarkGenerate(b *testing.B) {
	node, _ := NewNode(1)
	b.ReportAllocs()
	for n := 0; n < b.N; n++{
		node.Generate()
	}
}


func BenchmarkGenerateParallel(b *testing.B){
	node, _ := NewNode(1)
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB){
		for pb.Next(){
			node.Generate()
		}
	})
}